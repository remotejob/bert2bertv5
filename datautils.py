import pandas as pd 
# Read data from file 'filename.csv' 
# (in the same directory that your python process is based)
# Control delimiters, rows, column names with read_csv (see later) 

#data = pd.read_csv("data/askanseval.csv")
# data = pd.read_csv("/home/juno/common/data/twitterdialogs/askans.tsv",sep="\t")
data = pd.read_csv("/home/juno/common/mltwitterv3/newrecords/askans.tsv",sep="\t")
  
# Preview the first 5 lines of the loaded data 
print(data.info())

dataclean =  data.dropna()
print(dataclean.info())

df = dataclean.sample(frac=1).reset_index(drop=True)

#df.to_csv('data/pdaskanseval.csv', index=False)
df.to_csv('data/pdaskans.csv', index=False)
