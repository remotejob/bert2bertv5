from transformers import AutoModelWithLMHead, AutoTokenizer,BertTokenizerFast
import os
# directory = "output/content/models/tweetGPTfi/weights/checkpoint-140000"
# pretrainedmodel = 'output/models/checkpoint-81000'
directory = "output/models/checkpoint-81000"
model = AutoModelWithLMHead.from_pretrained(directory)
# tokenizer = AutoTokenizer.from_pretrained(directory)
tokenizer = BertTokenizerFast.from_pretrained(directory)
out = "bert2bertv5_v0"
os.makedirs(out, exist_ok=True)
model.save_pretrained(out)
tokenizer.save_pretrained(out)