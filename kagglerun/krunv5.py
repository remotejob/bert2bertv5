# ApPW0jmKXzW83jDVDPpDdkLh4
# https://www.kaggle.com/pinooxd/gpt2-chatbot/notebook
# https://anubhav20057.medium.com/step-by-step-guide-abstractive-text-summarization-using-roberta-e93978234a90 It used!!

import os

# os.environ["HF_HOME"] = "/mnt/mldisk/cache/"
# os.environ['TRANSFORMERS_CACHE'] = '/mnt/mldisk/cache/'
# New
# os.system('pip install --upgrade git+https://github.com/intake/filesystem_spec')
# os.system('pip install -U git+git://github.com/huggingface/transformers')
# os.system('pip install --upgrade datasets')
# os.system('git clone https://github.com/huggingface/transformers')

os.system("pip install -U transformers")
os.system("pip install -U rouge_score")
os.system("pip install -U sacrebleu")
os.system("pip install -U wandb")
# os.system("pip install py7zr")
# os.system("pip install -U transformers==4.11.3")
# os.system('pip install -U pandas')

import wandb
import transformers
from transformers import AutoTokenizer,BertTokenizer,DistilBertTokenizerFast,BertTokenizerFast,AutoConfig,EncoderDecoderConfig
from datasets import load_dataset
from transformers import EncoderDecoderModel
from datasets import load_from_disk,load_metric
import torch
from torch.utils.data import DataLoader
from tqdm.notebook import tqdm
from transformers import Seq2SeqTrainer,Seq2SeqTrainingArguments, TrainerCallback
from datetime import datetime



# print("transformers.version",transformers.__version__)

# pretrainedmodel = 'TurkuNLP/bert-base-finnish-cased-v1'
# pretrainedmodel = '/kaggle/input/fidistillationsckpoint'
# pretrainedmodel = '../input/ckpoint'
# pretrainedmodel = '/kaggle/input/juhakalastusckpoint'
# pretrainedmodel = 'remotejob/bert2bertv4_v2'
pretrainedmodel = '/kaggle/input/markorahatckpoint'
# pretrainedmodel = 'distilroberta-base'
# pretrainedmodel = 'distilbert-base-cased'

# print("transformers.version",transformers.__version__)

os.system("wandb login 187758f8acd4c065f5f10a982e516222c1e743b1")

run = wandb.init(
    project="bert2bertv5",
    entity="remotejob2",
    config={
        "Tags": "markoNewRec81000"
    #     "epochs": epochs,
    #     "save_steps": save_steps,
    #     "eval_steps": eval_steps,
    #     "warmup_steps": warmup_steps,
    #     "best": best,
    #     "batch_size": batch_size,
    #     "acc": acc,
    },
)

# train_data = load_from_disk("/kaggle/input/bigtokenizeddatasets/train")
# val_data = load_from_disk("/kaggle/input/bigtokenizeddatasets/eval")

# train_data = load_from_disk("/kaggle/input/bigdistillationsdatasets/train")
# val_data = load_from_disk("/kaggle/input/bigdistillationsdatasets/eval")

# KALASTUS
# train_data = load_from_disk("/kaggle/input/bigjuhakalastusdatasets/train")
# val_data =load_from_disk("/kaggle/input/bigjuhakalastusdatasets/eval")

# MARKO
train_data = load_from_disk("/kaggle/input/bigmarkorahatdatasets/train")
val_data =load_from_disk("/kaggle/input/bigmarkorahatdatasets/eval")


train_data.set_format(
    type="torch", columns=["input_ids", "attention_mask","labels"],

)

val_data.set_format(
    type="torch", columns=["input_ids", "attention_mask","labels"],

)

# train_data = load_from_disk("/kaggle/input/bigmarkorahatdatasets/train" )
# val_data = load_from_disk("/kaggle/input/bigmarkorahatdatasets/validation")

# tokenizer = AutoTokenizer.from_pretrained(pretrainedmodel)
# tokenizer = DistilBertTokenizerFast.from_pretrained(pretrainedmodel)
tokenizer = BertTokenizerFast.from_pretrained(pretrainedmodel)

encoder_max_length=63
decoder_max_length=31

batch_size=140 #128 #140 at start 96/128 after

timeoutdiff = 650 #650-10.53h


# train_data = train_data.select(range(2500000)) #2500000
# val_data = val_data.select(range(10000)) #5000

# print(train_data)



# NEW STYLE
# config_encoder = AutoConfig.from_pretrained(pretrainedmodel)
# config_decoder = AutoConfig.from_pretrained(pretrainedmodel)
# config_decoder_config= EncoderDecoderConfig.from_encoder_decoder_configs(config_encoder, config_decoder)
# model = EncoderDecoderModel(config=config_decoder_config)
# config_encoder = model.config.encoder
# config_decoder  = model.config.decoder

# model.save_pretrained('my-model')
# encoder_decoder_config = EncoderDecoderConfig.from_pretrained(pretrainedmodel)

#From check poin
encoder_decoder_config =AutoConfig.from_pretrained(pretrainedmodel)
model = EncoderDecoderModel.from_pretrained(pretrainedmodel, config=encoder_decoder_config)



# model = EncoderDecoderModel.from_encoder_decoder_pretrained(pretrainedmodel, pretrainedmodel)

# _model = EncoderDecoderModel.from_pretrained(pretrainedmodel)
# _model.encoder.save_pretrained("./encoder")
# _model.decoder.save_pretrained("./decoder")
# OLD style
# model= EncoderDecoderModel.from_encoder_decoder_pretrained("./encoder", "./decoder", tie_encoder_decoder=True)
# model= EncoderDecoderModel.from_encoder_decoder_pretrained(pretrainedmodel, pretrainedmodel,tie_encoder_decoder=True)
# encoder_decoder_config = EncoderDecoderConfig.from_pretrained(pretrainedmodel)
# model= EncoderDecoderModel.from_encoder_decoder_pretrained(pretrainedmodel,config=encoder_decoder_config)
# # model.config = _model.config

model.config.decoder_start_token_id = tokenizer.cls_token_id
model.config.eos_token_id = tokenizer.sep_token_id
model.config.pad_token_id = tokenizer.pad_token_id
model.config.vocab_size = model.config.encoder.vocab_size

model.config.max_length = 40
model.config.early_stopping = True
model.config.no_repeat_ngram_size = 3
model.config.length_penalty = 2.0
model.config.num_beams = 4
model.config.vocab_size = model.config.encoder.vocab_size

rouge = load_metric("rouge")

def compute_rouge(pred_ids, label_ids):
    pred_str = tokenizer.batch_decode(pred_ids, skip_special_tokens=True)
    label_ids[label_ids == -100] = tokenizer.pad_token_id
    label_str = tokenizer.batch_decode(label_ids, skip_special_tokens=True)

    rouge_output = rouge.compute(predictions=pred_str, references=label_str, rouge_types=["rouge2"])["rouge2"].mid

    return {
        "rouge2_precision": round(rouge_output.precision, 4),
        "rouge2_recall": round(rouge_output.recall, 4),
        "rouge2_fmeasure": round(rouge_output.fmeasure, 4),
    }


optimizer = torch.optim.AdamW(model.parameters(), lr=5e-5)

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

model.to(device)

training_args = Seq2SeqTrainingArguments(
    evaluation_strategy="steps",
    output_dir="./models",
    per_device_train_batch_size=batch_size,
    per_device_eval_batch_size=batch_size,
    predict_with_generate=False,
    # evaluate_during_training=True,
    num_train_epochs=30,
    ignore_data_skip=False,
    do_train=True,
    do_eval=True,
    logging_steps=500,
    save_steps=1000,
    eval_steps=1000,
    warmup_steps=2000,
    save_total_limit=1,
    fp16=True,
    dataloader_num_workers=2,
    eval_accumulation_steps=1,
    prediction_loss_only=True,
    # resume_from_checkpoint = pretrainedmodel,
    # overwrite_output_dir=True,kaggle kernels push -p kagglerun/
    # prediction_loss_only=True insted of eval_accumulation_steps ?
    # gradient_checkpointing=True, #not supported !
)


class MyCallback(TrainerCallback):
    def on_train_begin(self, args, state, control, **kwargs):
        print("Starting training !!!")
        self.starttime = datetime.now()

    def on_epoch_end(self, args, state, control, **kwargs):
        print("on_epoch_end !!!")

    def on_save(self, args, state, control, **kwargs):
        print("Save checkpoint !!!")
        diff = datetime.now() - self.starttime
        diff_in_minutes = diff.total_seconds() / 60
        print("diff_in_minutes !!!", diff_in_minutes)
        if diff_in_minutes > timeoutdiff:
            print("STOP !!!")
            control.should_training_stop = True
        else:
            print("OK not STOP")


trainer = Seq2SeqTrainer(
    model=model,
    args=training_args,
    compute_metrics=compute_rouge,
    train_dataset=train_data,
    eval_dataset=val_data,
    tokenizer=tokenizer,
    callbacks=[MyCallback],
)

trainer.train(resume_from_checkpoint=pretrainedmodel)
# trainer.train()

