conda activate bert2bertv5 && export KAGGLE_CONFIG_DIR=/home/juno/kagglemarkorahat
DATA_________________________________
head -n 2000000 data/askans.tsv > data/2000000askans.tsv
#edit
python datautils.py
awk 'NR%100==0' data/pdaskans.csv > data/pdaskans_eval.csv
#batch SIZE!!!

#export LIBTORCH=$HOME/repos/pytorch/libtorch
#export LD_LIBRARY_PATH=${LIBTORCH}/lib:$LD_LIBRARY_PATH
unset LIBTORCH && unset LD_LIBRARY_PATH && python savetokenizeddataset.py




cp kdatasets/dataset-metadata.markorahat.json  bigtokenizeddatasets/dataset-metadata.json
#kaggle datasets create -r zip -p bigdatasets/ 
kaggle datasets version -r zip -p bigtokenizeddatasets/ -m "Updated data big"



#DOWNLOAD
----------------------------------------
rm -rf lastoutput && mv output lastoutput && mkdir output
kaggle kernels output markorahat/krunv5 -p output/
# python exportmodel.py
rm -rf output/models/best_model && rm -rf output/models/runs
# cp kdatasets/dataset-metadata.markorahat.ckpoint.json bert2bertv5v0/dataset-metadata.json
cp kdatasets/dataset-metadata.markorahat.ckpoint.json output/models/dataset-metadata.json && kaggle datasets version -r zip -p output/models -m "Updated"
# cp bert-base-finnish-cased-v1/bert-base-finnish-cased-v1/vocab.txt output/modelv5/
# cp distilroberta-base/*.txt output/modelv5/
# cp distilroberta-base/vocab.json output/modelv5/
#kaggle datasets create -r zip -p bert2bertv5v0
# kaggle datasets version -r zip -p bert2bertv5v0 -m "Updated data 1"
----------------------
cp kernelgpu/kernel-metadata.markorahat.json kagglerun/kernel-metadata.json && kaggle kernels push -p kagglerun/
