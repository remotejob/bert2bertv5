
from transformers import EncoderDecoderModel,BertTokenizerFast
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
import uvicorn

from pydantic import BaseModel

# pretrainedmodel = 'TurkuNLP/bert-base-finnish-cased-v1'
pretrainedmodel = 'remotejob/bert2bertv5_v0'

tokenizer = BertTokenizerFast.from_pretrained(pretrainedmodel)
tokenizer.bos_token = tokenizer.cls_token
tokenizer.eos_token = tokenizer.sep_token

model = EncoderDecoderModel.from_pretrained(pretrainedmodel)

origins = ["*"]


class Itemask(BaseModel):
    ask: str


def get_reply(model,tokenizer, ask):

    inputs = tokenizer(ask, padding="max_length",truncation=True, max_length=25, return_tensors="pt")
    input_ids = inputs.input_ids
    attention_mask = inputs.attention_mask
    numwords = len(ask.split())
    if numwords < 4:
        penalty = 0.8
    else:
        penalty = 1.5
    outputs = model.generate(input_ids, attention_mask=attention_mask,length_penalty=penalty,early_stopping= True,num_beams=numwords,top_k=50,top_p=0.95)
    output_str = tokenizer.batch_decode(outputs, skip_special_tokens=True)

    return output_str[0]

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

@app.get("/")
def read_root():
    return {"Hello": "World"}

@app.post("/translator/translate")
def predict(item: Itemask):
    ask = item.ask
    ans =  get_reply(model,tokenizer, ask)
    return  {'Answer':ans} 

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=5000, log_level="info", reload=False)
