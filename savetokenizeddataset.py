from transformers import BertTokenizerFast,AutoTokenizer
from datasets import Dataset,concatenate_datasets
from torch.utils.data import IterableDataset
from datasets import load_dataset

import pandas as pd

batch_size = 112  # 256/275/224 for finns 100 Roberta 128 multiling 186 #27 #64
encoder_max_length = 63
decoder_max_length = 31
# pretrainedmodel = 'TurkuNLP/bert-base-finnish-cased-v1'
# pretrainedmodel = 'distilbert-base-uncased'
pretrainedmodel = 'remotejob/bert2bertv4_v2'

#pretrainedmodel = 'remotejob/bert2bertv4_v1'

# tokenizer = BertTokenizerFast.from_pretrained(pretrainedmodel)
tokenizer = AutoTokenizer.from_pretrained(pretrainedmodel)
# tokenizer.bos_token = tokenizer.cls_token
# tokenizer.eos_token = tokenizer.sep_token

def process_data_to_model_inputs(batch):
    # tokenize the inputs and labels

    inputs = tokenizer(batch["ask"], padding="max_length",
                       truncation=True, max_length=encoder_max_length)
    outputs = tokenizer(batch["answer"], padding="max_length",
                        truncation=True, max_length=decoder_max_length)

    batch["input_ids"] = inputs.input_ids
    batch["attention_mask"] = inputs.attention_mask
    # batch["decoder_input_ids"] = outputs.input_ids
    # batch["decoder_attention_mask"] = outputs.attention_mask
    batch["labels"] = outputs.input_ids.copy()

    # because RoBERTa automatically shifts the labels, the labels correspond exactly to `decoder_input_ids`.
    # We have to make sure that the PAD token is ignored
    batch["labels"] = [[-100 if token == tokenizer.pad_token_id else token for token in labels]
                       for labels in batch["labels"]]
    return batch


# train_data = load_dataset("csv", data_files=['data/pdaskans.csv'],column_names=["ask","answer"])
train_data = load_dataset('csv', data_files={'train': ['data/pdaskans.csv'],
                                              'eval': 'data/pdaskans_eval.csv'},column_names=["ask","answer"])

# train_data = train_data['train'].select(range(5000000))

train_data_train = train_data['train'].map(
    process_data_to_model_inputs,
    batched=True,
    batch_size=batch_size,
    num_proc = 2,
    keep_in_memory = True,
    remove_columns=["ask", "answer"]
)


train_data_train.save_to_disk("bigtokenizeddatasets/train")


# eval_data = train_data['eval']



eval_data = train_data['eval'].map(
    process_data_to_model_inputs,
    batched=True,
    batch_size=batch_size,
    num_proc = 4,
    keep_in_memory = True,
    remove_columns=["ask", "answer"]
)


eval_data.save_to_disk("bigtokenizeddatasets/eval")









