#https://www.machinecurve.com/index.php/2021/02/16/easy-machine-translation-with-machine-learning-and-huggingface-transformers/
#https://www.gitmemory.com/issue/huggingface/transformers/8704/734644401
# from transformers import pipeline
from datasets import load_dataset
from transformers import EncoderDecoderModel,AutoTokenizer
from transformers import BertTokenizerFast


prod_data = load_dataset("csv", data_files=[
                                '/home/juno/common/fortest/testcased.txt'], delimiter="\t", column_names=["ask"])
#model = EncoderDecoderModel.from_pretrained("models/best_model")
#tokenizer = BertTokenizerFast.from_pretrained("models/best_model")

# model = EncoderDecoderModel.from_pretrained("bert2bertv5v0")
model = EncoderDecoderModel.from_pretrained("output/models/checkpoint-81000")
# tokenizer = BertTokenizerFast.from_pretrained("output/models/checkpoint-31000")
tokenizer = AutoTokenizer.from_pretrained("output/models/checkpoint-81000")
# tokenizer = AutoTokenizer.from_pretrained("bert2bertv5v0")

# model = EncoderDecoderModel.from_pretrained("remotejob/bert2bertv4_v2")
# tokenizer = BertTokenizerFast.from_pretrained("remotejob/bert2bertv4_v2")


f = open("0seq2seqres.txt", "w")
for ask in prod_data['train']['ask']:

    inputs = tokenizer(ask, padding="max_length",
                        truncation=True, max_length=30, return_tensors="pt")
    input_ids = inputs.input_ids
    attention_mask = inputs.attention_mask
    # outputs = model.generate(input_ids, attention_mask=attention_mask) #org
    # outputs = model.generate(input_ids, attention_mask=attention_mask,length_penalty=1.1,early_stopping= True,num_beams=8) #GOOG

    numwords = len(ask.split())
    if numwords < 4:
        penalty = 0.8
    else:
        penalty = 1.5


    outputs = model.generate(input_ids, attention_mask=attention_mask,length_penalty=penalty,early_stopping= True,num_beams=numwords,top_k=50,top_p=0.95)
    
    output_str = tokenizer.batch_decode(outputs, skip_special_tokens=True)
    print(ask + ' --> ' + output_str[0])
    f.write(ask + ' --> '+output_str[0]+'\n')

f.close()

    # print(ask)

    # tokenized_text = tokenizer.prepare_seq2seq_batch([ask], return_tensors='pt')
    # tokenized_text = tokenizer(ask, return_tensors="pt").input_ids
    # tokenized_text = tokenizer(ask, return_tensors="pt")
    # tokenized_text = tokenizer.prepare_seq2seq_batch(src_texts=[ask], return_tensors="pt")

    #     # do_sample: true,
    #     # num_beams: 1,
    #     # top_k: 50,
    #     # top_p: 0.95,
    #     # no_repeat_ngram_size: 2,
    #     # early_stopping: true,

    # # translation = model.generate(input_ids=tokenized_text.input_ids, num_return_sequences=1, num_beams=8, length_penalty=0.1)
    # # translation = model.generate(input_ids=tokenized_text.input_ids, num_return_sequences=1)
    # translation = model.generate(input_ids=tokenized_text.input_ids, num_return_sequences=1, num_beams=8,no_repeat_ngram_size=3)  #OK 
    # # translation = model.generate(input_ids=tokenized_text.input_ids, num_return_sequences=1, num_beams=12,no_repeat_ngram_size=2,length_penalty=1.1,early_stopping=True)
    # # translation = model.generate(input_ids=tokenized_text.input_ids, num_return_sequences=1, num_beams=1,no_repeat_ngram_size=2,length_penalty=1.1,top_k=50,top_p=0.95) #OK
    # # translation = model.generate(input_ids=tokenized_text.input_ids, num_return_sequences=1, num_beams=5,no_repeat_ngram_size=2,length_penalty=1.1,top_k=50,top_p=0.95)
    # # translation = model.generate(input_ids=tokenized_text.input_ids, num_return_sequences=1, num_beams=20,no_repeat_ngram_size=2,length_penalty=1.5,early_stopping=True)
    # # translation = model.generate(**tokenized_text)
    # # translated_text = tokenizer.batch_decode(translation, skip_special_tokens=True)[0]

    # translation = model.generate(input_ids=tokenized_text.input_ids, num_return_sequences=1, num_beams=8,no_repeat_ngram_size=3) #ORG

    # translated_text = tokenizer.decode(translation[0], skip_special_tokens=True)

    # print(ask + ' --> '+translated_text)
    # f.write(ask + ' --> '+translated_text+'\n')
