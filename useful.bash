conda update --all

# conda create -n bert2bertv5 python=3.8
# conda activate bert2bertv5

# pip install -U catalyst

# pip install -U torch
# pip install --upgrade git+https://github.com/intake/filesystem_spec
# pip install -U git+git://github.com/huggingface/transformers
pip install -U transformers
# pip install --upgrade datasets
# pip install -U nltk
# pip install -U rouge_score
# pip install -U sacrebleu
# pip install -U wandb
# pip install -U pandas

scp 192.168.1.202:/home/juno/gowork/src/gitlab.com/remotejob/mltwitterv3/data/*.txt ~/common/mltwitterv3/newrecords

paste -d "\t" ~/common/mltwitterv3/newrecords/ask.txt ~/common/mltwitterv3/newrecords/answer.txt >  ~/common/mltwitterv3/newrecords/askans.tsv

python datautils.py

awk 'NR%100==0' data/pdaskans.csv > data/pdaskans_eval.csv


#export model 

#huggingface-cli login only ones!

huggingface-cli repo create bert2bertv5_v0
cd ~/repos/huggingface.co/remotejob
git clone https://huggingface.co/remotejob/bert2bertv5_v0

cd bert2bertv5_v0
cp -a /home/juno/repos/gitlab.com/remotejob/bert2bertv5/bert2bertv5_v0/* .
echo "hello" >> README.md
git add . && git commit -m "Update from $USER"

curl -X POST localhost:5000/translator/translate  -H 'Content-Type: application/json' -d '{"ask":"Ethän sää uskalla"}'
curl -X POST 143.198.23.106:5000/translator/translate  -H 'Content-Type: application/json' -d '{"ask":"Ethän sää uskalla"}'

